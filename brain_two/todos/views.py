from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import ListForm, ItemForm


# Create your views here.
def todo_list_list(request):
    todo_object = TodoList.objects.all()
    context = {
        "todo_list_list": todo_object,
    }
    return render(request, "todos/list.html", context)

def show_todo_list_detail(request, id):
    todo_list_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todo_list_detail,
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = ListForm(request.POST)
        if form.is_valid():
            todo_object = form.save(False)
            todo_object.save()
        return redirect("todo_list_list")
    else:
        form = ListForm()
        context = {
            "form": form,
        }
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    object = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = ListForm(request.POST, instance=object)
        if form.is_valid():
            form.save()
        return redirect("todo_list_list")
    else:
        form = ListForm(instance=object)
        context = {
            "list_object": object,
            "form": form,
        }
    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
    delete_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            form = form.save()
        return redirect("show_todo_list_detail", id=form.list.id)
    else:
        form = ItemForm()
        context = {
            "form": form,
        }
    return render(request, "todos/itemscreate.html", context)


def todo_item_update(request, id):
    object = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=object)
        if form.is_valid():
            form.save()
        return redirect("todo_list_list")
    else:
        form = ItemForm(instance=object)
        context = {
            "update_object": object,
            "form": form,
        }
    return render(request, "todos/itemsedit.html", context)
